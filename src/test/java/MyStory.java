import org.jbehave.core.embedder.Embedder;

import java.util.Arrays;
import java.util.List;

public class MyStory {
    private static Embedder embedder = new Embedder();
    private static List<String> storyPaths = Arrays
            .asList("my.story");

    public static void main(String[] args) {
        embedder.candidateSteps().add(new ExampleSteps());
        embedder.runStoriesAsPaths(storyPaths);
    }
}
